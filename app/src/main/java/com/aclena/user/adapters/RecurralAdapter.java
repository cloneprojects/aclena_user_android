package com.aclena.user.adapters;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.makeramen.roundedimageview.RoundedImageView;
import com.aclena.user.R;
import com.aclena.user.Volley.ApiCall;
import com.aclena.user.Volley.VolleyCallback;
import com.aclena.user.activities.ViewSchedule;
import com.aclena.user.helpers.UrlHelper;
import com.aclena.user.helpers.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by karthik on 07/10/17.
 */
public class RecurralAdapter extends RecyclerView.Adapter<RecurralAdapter.MyViewHolder> {
    private Context context;
    private JSONArray subCategories;
    private String TAG = RecurralAdapter.class.getSimpleName();

    public RecurralAdapter(Context context, JSONArray subCategories) {
        this.context = context;
        this.subCategories = subCategories;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_recurral, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        try {
            Utils.log(TAG, "values: " + subCategories.optJSONObject(position));
            String providerName, prvoider_pic, providerMobile;
            providerName = subCategories.optJSONObject(position).optString("name");
            prvoider_pic = subCategories.optJSONObject(position).optString("image");
            providerMobile = subCategories.optJSONObject(position).optString("mobile");

            holder.providerName.setText(providerName);
            holder.providerMobile.setText(providerMobile);
            holder.serviceName.setText(subCategories.optJSONObject(position).optString("sub_category_name"));
            holder.nextServiceDate.setText(subCategories.optJSONObject(position).optString("booking_date"));
            holder.serviceTiming.setText(subCategories.optJSONObject(position).optString("recurrent_type"));


            Glide.with(context).load(prvoider_pic).placeholder(context.getResources().getDrawable(R.drawable.profile_pic)).dontAnimate().into(holder.providerPic);


        } catch (Exception e) {
            e.printStackTrace();
        }

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String bookingId = subCategories.optJSONObject(position).optString("id");

                showCancelDialog(bookingId);
            }
        });


    }


    private void showCancelDialog(final String id) {
        final Dialog dialog = new Dialog(context);
        dialog.setCancelable(true);
        dialog.setCanceledOnTouchOutside(true);
        dialog.setContentView(R.layout.cancel_dialog);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        Window window = dialog.getWindow();
        window.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
        window.setGravity(Gravity.CENTER);
        dialog.show();
        TextView yesButton, noButton;
        yesButton = (TextView) dialog.findViewById(R.id.yesButton);
        noButton = (TextView) dialog.findViewById(R.id.noButton);

        yesButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                canceBookings(id);
            }
        });

        noButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
    }


    private void canceBookings(String id) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("id", id);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        ApiCall.PostMethodHeaders(context, UrlHelper.CANCEL_RECURRAL, jsonObject, new VolleyCallback() {
            @Override
            public void onSuccess(JSONObject response) {
                ViewSchedule.getData(context);
            }
        });
    }


    @Override
    public int getItemCount() {
        return subCategories.length();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {
        TextView serviceName, serviceTiming, nextServiceDate;
        RoundedImageView providerPic;
        TextView providerName, providerMobile;


        MyViewHolder(View view) {
            super(view);
            providerPic = view.findViewById(R.id.providerPic);
            providerName = view.findViewById(R.id.providerName);
            providerMobile = view.findViewById(R.id.providerMobile);
            serviceTiming = view.findViewById(R.id.serviceTiming);
            serviceName = view.findViewById(R.id.serviceName);

            nextServiceDate = view.findViewById(R.id.nextServiceDate);

        }
    }
}
