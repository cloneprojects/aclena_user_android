package com.aclena.user.adapters;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.aclena.user.R;
import com.aclena.user.activities.AppSettings;
import com.aclena.user.activities.MainActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by user on 23-10-2017.
 */

public class RecurringTimesAdapter extends RecyclerView.Adapter<RecurringTimesAdapter.MyViewHolder> {
    private Context context;
    private JSONArray timeSlots;
    private String TAG = RecurringTimesAdapter.class.getSimpleName();

    public RecurringTimesAdapter(Context context, JSONArray timeSlots) {
        this.context = context;
        this.timeSlots = timeSlots;
    }

    @Override
    public RecurringTimesAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_time_slots, parent, false);

        return new RecurringTimesAdapter.MyViewHolder(itemView);
    }


    @Override
    public void onBindViewHolder(final RecurringTimesAdapter.MyViewHolder holder, final int position) {
        try {
            final AppSettings appSettings = new AppSettings(context);

            final JSONObject singleSlot = timeSlots.getJSONObject(position);
//            if (SelectTimeAndAddressActivity.isTodaySelected) {
//                String fromTime = singleSlot.optString("toTime");
//
//                Date date = new SimpleDateFormat("HH:mm:ss", Locale.ENGLISH).parse(fromTime);
//
//                SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
//                String currentDateandTime = sdf.format(Calendar.getInstance().getTime());
//                Date currentdate = new SimpleDateFormat("HH:mm:ss", Locale.ENGLISH).parse(currentDateandTime);
//
//
//                long currenttime = currentdate.getTime();
//                long serviceTime = date.getTime();
//                Log.d(TAG, "serviceTIme: " + currenttime + "/" + serviceTime);
//
//
//            } else {
//                holder.itemView.setVisibility(View.VISIBLE);
//
//                holder.itemView.setLayoutParams(new RecyclerView.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
//
//                if (position==timeSlots.length()-1)
//                {
//                    EventBus.getDefault().post(new onSelected("all"));
//                }
//
//            }
            Log.d(TAG, "onBindViewHolder: " + singleSlot);
            holder.timeSlot.setText(singleSlot.optString("timing"));
            if (singleSlot.optString("selected").equalsIgnoreCase("true")) {
                holder.timeSlot.setBackgroundResource(R.drawable.blue_bg);
                holder.timeSlot.setTextColor(Color.parseColor("#ffffff"));
                MainActivity.isTimeSelected = true;
                appSettings.setSelectedRecurralTimeSlot(singleSlot.optString("id"));
                appSettings.setSelectedRecurralTimeText(holder.timeSlot.getText().toString());
            } else {
                holder.timeSlot.setBackgroundResource(R.drawable.gray_bg);
                holder.timeSlot.setTextColor(Color.parseColor("#1d1d1d"));
            }

            holder.timeSlot.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {


                    if (singleSlot.optString("selected").equalsIgnoreCase("true")) {

                    } else {
                        for (int i = 0; i < timeSlots.length(); i++) {
                            if (i != position) {
                                timeSlots.optJSONObject(i).remove("selected");
                                try {
                                    timeSlots.optJSONObject(i).put("selected", "false");
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            } else {
                                timeSlots.optJSONObject(i).remove("selected");
                                try {
                                    timeSlots.optJSONObject(i).put("selected", "true");
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }

                        }

                    }
                    notifyDataSetChanged();
                }
            });
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    @Override
    public int getItemCount() {
        return timeSlots.length();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {
        TextView timeSlot;

        MyViewHolder(View view) {
            super(view);
            timeSlot = view.findViewById(R.id.timeSlot);
        }
    }
}