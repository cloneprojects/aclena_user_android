package com.aclena.user.helpers;

/**
 * Created by user on 23-10-2017.
 */

public class UrlHelper {

    public static String BASE = "http://35.178.27.62/";
    public static String BASE_URL = "http://35.178.27.62/Aclena/public/api/";
    public static String SIGN_UP = BASE_URL + "signup";
    public static String SIGN_IN = BASE_URL + "userlogin";
    public static String FORGOT_PASSWORD = BASE_URL + "forgotpassword";
    public static String RESET_PASSWORD = BASE_URL + "resetpassword";
    public static String HOME_DASH_BOARD = BASE_URL + "homedashboard";
    public static String APP_SETTINGS = BASE_URL + "appsettings";
    public static String LIST_ADDRESS = BASE_URL + "listaddress";
    public static String ADD_ADDRESS = BASE_URL + "addaddress";
    public static String LIST_PROVIDERS = BASE_URL + "listprovider";
    public static String NEW_BOOKINGS = BASE_URL + "newbooking";
    public static String VIEW_BOOKINGS = BASE_URL + "view_bookings";
    public static String GET_PROVIDER_LOCATION = BASE_URL + "getproviderlocation";
    public static String VIEW_PROFILE = BASE_URL + "viewprofile";
    public static String UPDATE_DEVICE_TOKEN = BASE_URL + "updatedevicetoken";
    public static String REVIEW = BASE_URL + "review";
    public static String PAYMENT = BASE_URL + "pay";
    public static String GET_RECURRAL = BASE_URL + "list_provider_schedules";
    public static String PAYMENT_METHODS = BASE_URL + "list_payment_methods";
    public static String CHANGE_PASSWORD = BASE_URL + "changepassword";
    public static String LIST_SUB_CATEGORY = BASE_URL + "list_subcategory";
    public static String STRIPE_PAYMENT = BASE_URL + "stripe";
    public static String UPLOAD_IMAGE = BASE + "Aclena/public/admin/imageupload";
    public static String CANCEL_BOOKING = BASE_URL + "cancelbyuser";
    public static String BOOK_RECURRENT = BASE_URL + "book_recurrent";
    public static String REJECT_RECURRENT = BASE_URL + "reject_recurral";
    public static String GET_PROVIDER_TIME_SLOTS = BASE_URL + "get_provider_timeslots";
    public static String ABOUT_US = BASE + "Aclena/public/admin/aboutus";
    public static String FAQ = BASE + "Aclena/public/admin/faq";
    public static String TERMS = BASE + "Aclena/public/admin/terms";
    public static String UPDATE_PROFILE = BASE_URL + "updateprofile";
    public static String DELETE_ADDRESS = BASE_URL + "deleteaddress";
    public static String UPDATE_ADDRESS = BASE_URL + "updateaddress";
    public static String CANCEL_REQUEST = BASE_URL + "cancel_request";
    public static String SOCIAL_LOGIN = BASE_URL + "sociallogin";
    public static String SOCKET_URL = "http://35.178.27.62:3000";

    public static String RANDOM_BOOKING = BASE_URL + "random_request";
    public static String CANCEL_RECURRAL = BASE_URL + "cancel_schedule";
}
