package com.aclena.user.activities;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.aclena.user.R;
import com.aclena.user.Volley.ApiCall;
import com.aclena.user.Volley.VolleyCallback;
import com.aclena.user.helpers.UrlHelper;
import com.aclena.user.helpers.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class ConfirmBookingActivity extends AppCompatActivity {
    TextView providerName;
    TextView addressTitle;
    TextView addressText;
    RatingBar ratingView;
    Button confirmBooking;
    TextView pricing, dateValue, timeValue;
    String selected_id;
    ImageView backButton;
    ImageView mapData;
    TextView serviceName;
    ImageView providerPic;
    TextView phoneNumber;
    AppSettings appSettings = new AppSettings(ConfirmBookingActivity.this);
    private String TAG = ConfirmBookingActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_confirm_booking);

        initViews();
        initListners();


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setStatusBarColor(Color.BLACK);
        }


    }

    private void initListners() {
        confirmBooking.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ApiCall.PostMethodHeaders(ConfirmBookingActivity.this, UrlHelper.NEW_BOOKINGS, getInputs(), new VolleyCallback() {
                    @Override
                    public void onSuccess(JSONObject response) {
                        showSuccessDialog();
                    }
                });

            }
        });


        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

    }

    private JSONObject getInputs() {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("service_sub_category_id", appSettings.getSelectedSubCategory());
            jsonObject.put("time_slot_id", appSettings.getSelectedTimeSlot());
            jsonObject.put("date", appSettings.getSelectedDate());
            jsonObject.put("provider_id", selected_id);
            jsonObject.put("address_id", appSettings.getSelectedAddressId());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonObject;
    }

    private void showSuccessDialog() {
        try {
            AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(ConfirmBookingActivity.this);
            LayoutInflater inflater = getLayoutInflater();
            View dialogView = inflater.inflate(R.layout.alert_booking_confirmed, null);
            dialogBuilder.setView(dialogView);

            Button okay = dialogView.findViewById(R.id.okay);
            final AlertDialog alertDialog = dialogBuilder.create();
            alertDialog.show();
            alertDialog.setCancelable(false);
            okay.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    alertDialog.dismiss();
                    moveToMainActivity();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void moveToMainActivity() {
        Intent intent = new Intent(ConfirmBookingActivity.this, MainActivity.class);
        intent.putExtra("type", "success");
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        finish();
    }

    private void initViews() {
        providerName = (TextView) findViewById(R.id.providerName);
        addressTitle = (TextView) findViewById(R.id.addressTitle);
//        ratingView = (RatingBar) findViewById(R.id.ratingView);
        dateValue = (TextView) findViewById(R.id.dateValue);
        timeValue = (TextView) findViewById(R.id.timeValue);
        addressText = (TextView) findViewById(R.id.addresstText);
        pricing = (TextView) findViewById(R.id.pricing);
        serviceName = (TextView) findViewById(R.id.serviceName);
        confirmBooking = (Button) findViewById(R.id.confirmBooking);
        providerPic = (ImageView) findViewById(R.id.providerPic);
        mapData = (ImageView) findViewById(R.id.mapData);

        backButton = findViewById(R.id.backButton);

        phoneNumber = (TextView) findViewById(R.id.phoneNumber);

        providerName.setText(getIntent().getStringExtra("providerName"));
        phoneNumber.setText(getIntent().getStringExtra("providerMobile"));
        pricing.setText(getIntent().getStringExtra("pricing"));
//        ratingView.setRating(Float.parseFloat(getIntent().getStringExtra("providerRating")));
        dateValue.setText(appSettings.getSelectedDate());
        timeValue.setText(appSettings.getSelectedTimeText());
        serviceName.setText(appSettings.getSelectedSubCategoryName());
        selected_id = getIntent().getStringExtra("selected_id");

        SpannableString spannableString = new SpannableString(appSettings.getSelectedAddressTitle());
        spannableString.setSpan(new ForegroundColorSpan(Color.BLACK), 0, appSettings.getSelectedAddressTitle().length(), 0);

        addressTitle.setText(new StringBuilder()
                .append(getResources().getString(R.string.address))
                .append(": ").append("(").append(appSettings.getSelectedAddressTitle()).append(")"));

        Glide.with(ConfirmBookingActivity.this).load(getIntent().getStringExtra("providerPic")).placeholder(getResources().getDrawable(R.drawable.profile_pic)).into(providerPic);
        getStaticMap();
    }

    private void getStaticMap() {

//        String urls="https://maps.googleapis.com/maps/api/staticmap?key="+getResources().getString(R.string.google_maps_key)+"&markers=color:red%7C"+lat+","+longg+"&center="+lat+","+longg+"&zoom=15&format=jpeg&maptype=roadmap&size=512x512&sensor=false";
        String urls = "https://maps.googleapis.com/maps/api/staticmap?key=" + getResources().getString(R.string.google_maps_key) + "&center=" + appSettings.getSelectedlat() + "," + appSettings.getSelectedLong() + "&zoom=15&format=jpeg&maptype=roadmap&size=512x512&sensor=false&style=feature:administrative|element:geometry|color:0x1d1d1d|weight:1&style=feature:administrative|element:labels.text.fill|color:0x93a6b5&style=feature:landscape|color:0xeff0f5&style=feature:landscape|element:geometry|color:0xdde3e3|visibility:simplified|weight:0.5&style=feature:landscape|element:labels|color:0x1d1d1d|visibility:simplified|weight:0.5&style=feature:landscape.natural.landcover|element:geometry|color:0xfceff9&style=feature:poi|element:geometry|color:0xeeeeee&style=feature:poi|element:labels|visibility:off|weight:0.5&style=feature:poi|element:labels.text|color:0x505050|visibility:off&style=feature:poi.attraction|element:labels|visibility:off&style=feature:poi.attraction|element:labels.text|color:0xa6a6a6|visibility:off&style=feature:poi.business|element:labels|visibility:off&style=feature:poi.business|element:labels.text|color:0xa6a6a6|visibility:off&style=feature:poi.government|element:labels|visibility:off&style=feature:poi.government|element:labels.text|color:0xa6a6a6|visibility:off&style=feature:poi.medical|element:labels.text|color:0xa6a6a6|visibility:simplified&style=feature:poi.park|element:geometry|color:0xa9de82&style=feature:poi.park|element:labels.text|color:0xa6a6a6|visibility:simplified&style=feature:poi.place_of_worship|element:labels.text|color:0xa6a6a6|visibility:simplified&style=feature:poi.school|element:labels.text|color:0xa6a6a6|visibility:simplified&style=feature:poi.sports_complex|element:labels.text|color:0xa6a6a6|visibility:simplified&style=feature:road|element:geometry|color:0xffffff&style=feature:road|element:labels.text|color:0xc0c0c0|visibility:simplified|weight:0.5&style=feature:road|element:labels.text.fill|color:0x000000&style=feature:road.highway|element:geometry|color:0xf4f4f4|visibility:simplified&style=feature:road.highway|element:labels.text|color:0x1d1d1d|visibility:simplified&style=feature:road.highway.controlled_access|element:geometry|color:0xf4f4f4&style=feature:transit|element:geometry|color:0xc0c0c0&style=feature:water|element:geometry|color:0xa5c9e1";
        Utils.log(TAG, "url: " + urls);
//        String url = "http://maps.google.com/maps/api/staticmap?center="+lat+","+longg+"&zoom=15&size=512x512&sensor=false";

        Glide.with(ConfirmBookingActivity.this).load(urls).listener(new RequestListener<String, GlideDrawable>() {
            @Override
            public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                return false;
            }

            @Override
            public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                addressText.setText(appSettings.getSelectedAddress());
                return false;
            }
        }).into(mapData);


    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }
}
