package com.aclena.user.activities;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.CountDownTimer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.aclena.user.R;
import com.aclena.user.Volley.ApiCall;
import com.aclena.user.Volley.VolleyCallback;
import com.aclena.user.helpers.UrlHelper;
import com.aclena.user.helpers.Utils;
import com.skyfishjy.library.RippleBackground;

import org.json.JSONException;
import org.json.JSONObject;

import java.net.URISyntaxException;

import de.hdodenhof.circleimageview.CircleImageView;
import io.socket.client.IO;
import io.socket.client.Socket;
import io.socket.emitter.Emitter;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class SearchingActivity extends AppCompatActivity implements View.OnClickListener {
    CircleImageView staticImage;
    private String TAG = SearchingActivity.class.getSimpleName();
    AppSettings appSettings;
    TextView searchingTextView;
    RippleBackground rippleLayout;
    LinearLayout beforeSearch, afterSearch;
    private Socket socket;
    String radiusValue;
    EditText radiusEditText;
    Button confirmBooking, cancelBooking;
    boolean isRequestAccepted = false;
    private TextView dateValue, serviceName, timeValue, addressText;

    CountDownTimer countDownTimer = new CountDownTimer(30000, 1000) {

        public void onTick(long millisUntilFinished) {
            if (millisUntilFinished % 3 == 0) {

            }
            //here you can have your logic to set text to edittext
        }

        public void onFinish() {
//            if (!isRequestAccepted)
//            {
//                moveToMainActivity();
//            }
        }

    };
    private String booking_id;


    private void moveToMainActivity() {
        Intent intent = new Intent(SearchingActivity.this, MainActivity.class);
        intent.putExtra("type", "success");
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        finish();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setStatusBarColor(Color.BLACK);
        }
        setContentView(R.layout.activity_searching_activity);
        appSettings = new AppSettings(SearchingActivity.this);

        initSocket();
        initViews();
        initListeners();
        getStaticMap();
    }

    private void initListeners() {
        confirmBooking.setOnClickListener(this);
        cancelBooking.setOnClickListener(this);
    }

    private void initSocket() {
        IO.Options opts = new IO.Options();
        opts.forceNew = true;
        opts.reconnection = true;
        try {
            socket = IO.socket(UrlHelper.SOCKET_URL, opts);
        } catch (URISyntaxException e) {
            e.printStackTrace();
            Log.e("SOCKET.IO ", e.getMessage());
        }
        socket.connect();
        Log.e(TAG, "initSocket: connected");
        startListening();
    }

    private void initViews() {
        staticImage = (CircleImageView) findViewById(R.id.staticImage);
        rippleLayout = (RippleBackground) findViewById(R.id.rippleLayout);
        searchingTextView = (TextView) findViewById(R.id.searchingTextView);
        confirmBooking = (Button) findViewById(R.id.confirmBooking);
        cancelBooking = (Button) findViewById(R.id.cancelBooking);
        beforeSearch = (LinearLayout) findViewById(R.id.beforeSearch);
        afterSearch = (LinearLayout) findViewById(R.id.afterSearch);
        radiusEditText = (EditText) findViewById(R.id.radiusEditText);
        dateValue = (TextView) findViewById(R.id.dateValue);
        timeValue = (TextView) findViewById(R.id.timeValue);
        addressText = (TextView) findViewById(R.id.addresstText);
        serviceName = (TextView) findViewById(R.id.serviceName);


        setBeforeLayout();
    }

    private void setAfterLayout() {
        rippleLayout.startRippleAnimation();
        searchingTextView.setText(getResources().getString(R.string.looking_for) + " " + appSettings.getSelectedSubCategoryName() + " ...");

        beforeSearch.setVisibility(View.GONE);
        afterSearch.setVisibility(View.VISIBLE);
        addressText.setText(appSettings.getSelectedAddress());
        dateValue.setText(appSettings.getSelectedDate());
        timeValue.setText(appSettings.getSelectedTimeText());
        serviceName.setText(appSettings.getSelectedSubCategoryName());
        countDownTimer.start();
    }

    private void setBeforeLayout() {
        searchingTextView.setText("");
        beforeSearch.setVisibility(View.VISIBLE);
        afterSearch.setVisibility(View.GONE);
    }

    private void postRequest(String radius) throws JSONException {

        JSONObject jsonObject = new JSONObject();
        jsonObject.put("subcategory_id", appSettings.getSelectedSubCategory());
        jsonObject.put("category_id", appSettings.getCategoryId());
        jsonObject.put("time_slot_id", appSettings.getSelectedTimeSlot());
        jsonObject.put("date", appSettings.getSelectedDate());
        jsonObject.put("user_id", appSettings.getUserId());
        jsonObject.put("radius", radius);
        jsonObject.put("latitude", appSettings.getSelectedlat());
        jsonObject.put("longitude", appSettings.getSelectedLong());
        jsonObject.put("address_id", appSettings.getSelectedAddressId());

        Log.d(TAG, "postRequest: " + jsonObject);

        socket.emit("GetRandomRequest", jsonObject);

        setAfterLayout();


    }

    private void startListening() {
        socket.on("random_request_accepted-" + appSettings.getUserId(), new Emitter.Listener() {
            @Override
            public void call(Object... args) {

                Log.d(TAG, "requestResponse: " + args[0]);
                JSONObject response = (JSONObject) args[0];
//                isRequestAccepted=true;

                moveToMainActivity();


            }
        });

        socket.on("request_completed-" + appSettings.getUserId(), new Emitter.Listener() {
            @Override
            public void call(Object... args) {

                Log.d(TAG, "requestResponse: " + args[0]);
                JSONObject response = (JSONObject) args[0];
//                isRequestAccepted=true;
                showAlertDialog();


            }
        });


        socket.on("user_booking-" + appSettings.getUserId(), new Emitter.Listener() {
            @Override
            public void call(Object... args) {

                Log.d(TAG, "requestResponse: " + args[0]);
                JSONObject response = (JSONObject) args[0];

                booking_id = response.optString("booking_id");

            }
        });
    }

    private void showAlertDialog() {
        SearchingActivity.this.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                setBeforeLayout();

                final Dialog dialog = new Dialog(SearchingActivity.this);
                dialog.setCancelable(true);
                dialog.setCanceledOnTouchOutside(true);
                dialog.setContentView(R.layout.no_providers_found);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                Window window = dialog.getWindow();
                window.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
                window.setGravity(Gravity.CENTER);
                dialog.show();
                TextView yesButton, noButton;
                yesButton = (TextView) dialog.findViewById(R.id.yesButton);
                noButton = (TextView) dialog.findViewById(R.id.noButton);

                yesButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dialog.dismiss();

                    }
                });

                noButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dialog.dismiss();
                        moveToMainActivity();
                    }
                });


            }
        });

    }

    @Override
    protected void onPause() {
        super.onPause();
        socket.disconnect();
    }

    @Override
    protected void onResume() {
        super.onResume();
        socket.connect();
    }

    private JSONObject getInputs() {
        JSONObject jsonObject = new JSONObject();

        return jsonObject;
    }

    private void getStaticMap() {

//        String urls="https://maps.googleapis.com/maps/api/staticmap?key="+getResources().getString(R.string.google_maps_key)+"&markers=color:red%7C"+lat+","+longg+"&center="+lat+","+longg+"&zoom=15&format=jpeg&maptype=roadmap&size=512x512&sensor=false";
        String urls = "https://maps.googleapis.com/maps/api/staticmap?key=" + getResources().getString(R.string.google_maps_key) + "&center=" + appSettings.getSelectedlat() + "," + appSettings.getSelectedLong() + "&zoom=16&format=jpeg&maptype=roadmap&size=512x512&sensor=false&style=feature:administrative|element:geometry|color:0x1d1d1d|weight:1&style=feature:administrative|element:labels.text.fill|color:0x93a6b5&style=feature:landscape|color:0xeff0f5&style=feature:landscape|element:geometry|color:0xdde3e3|visibility:simplified|weight:0.5&style=feature:landscape|element:labels|color:0x1d1d1d|visibility:simplified|weight:0.5&style=feature:landscape.natural.landcover|element:geometry|color:0xfceff9&style=feature:poi|element:geometry|color:0xeeeeee&style=feature:poi|element:labels|visibility:off|weight:0.5&style=feature:poi|element:labels.text|color:0x505050|visibility:off&style=feature:poi.attraction|element:labels|visibility:off&style=feature:poi.attraction|element:labels.text|color:0xa6a6a6|visibility:off&style=feature:poi.business|element:labels|visibility:off&style=feature:poi.business|element:labels.text|color:0xa6a6a6|visibility:off&style=feature:poi.government|element:labels|visibility:off&style=feature:poi.government|element:labels.text|color:0xa6a6a6|visibility:off&style=feature:poi.medical|element:labels.text|color:0xa6a6a6|visibility:simplified&style=feature:poi.park|element:geometry|color:0xa9de82&style=feature:poi.park|element:labels.text|color:0xa6a6a6|visibility:simplified&style=feature:poi.place_of_worship|element:labels.text|color:0xa6a6a6|visibility:simplified&style=feature:poi.school|element:labels.text|color:0xa6a6a6|visibility:simplified&style=feature:poi.sports_complex|element:labels.text|color:0xa6a6a6|visibility:simplified&style=feature:road|element:geometry|color:0xffffff&style=feature:road|element:labels.text|color:0xc0c0c0|visibility:simplified|weight:0.5&style=feature:road|element:labels.text.fill|color:0x000000&style=feature:road.highway|element:geometry|color:0xf4f4f4|visibility:simplified&style=feature:road.highway|element:labels.text|color:0x1d1d1d|visibility:simplified&style=feature:road.highway.controlled_access|element:geometry|color:0xf4f4f4&style=feature:transit|element:geometry|color:0xc0c0c0&style=feature:water|element:geometry|color:0xa5c9e1";
        Utils.log(TAG, "url: " + urls);
//        String url = "http://maps.google.com/maps/api/staticmap?center="+lat+","+longg+"&zoom=15&size=512x512&sensor=false";

        Glide.with(SearchingActivity.this).load(urls).listener(new RequestListener<String, GlideDrawable>() {
            @Override
            public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                return false;
            }

            @Override
            public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
//                addressText.setText(appSettings.getSelectedAddress());
                return false;
            }
        }).into(staticImage);


    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }


    @Override
    public void onClick(View view) {
        if (view == confirmBooking) {
            radiusValue = radiusEditText.getText().toString().trim();
            if (!radiusValue.isEmpty()) {
                try {
                    postRequest(radiusValue);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                Toast.makeText(this, getString(R.string.enter_km_to_search), Toast.LENGTH_SHORT).show();
            }
        } else if (view == cancelBooking) {
            try {
                cancelBookings();
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    private void cancelBookings() throws JSONException {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("id", booking_id);
        ApiCall.PostMethodHeaders(SearchingActivity.this, UrlHelper.CANCEL_REQUEST, jsonObject, new VolleyCallback() {
            @Override
            public void onSuccess(JSONObject response) {
                setBeforeLayout();
            }
        });
    }
}
