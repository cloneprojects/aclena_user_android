package com.aclena.user.activities;

import android.content.Context;
import android.graphics.Color;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;

import com.aclena.user.R;
import com.aclena.user.Volley.ApiCall;
import com.aclena.user.Volley.VolleyCallback;
import com.aclena.user.adapters.RecurralAdapter;
import com.aclena.user.helpers.UrlHelper;

import org.json.JSONArray;
import org.json.JSONObject;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class ViewSchedule extends AppCompatActivity {
    public static RecyclerView scheduleRecyclerView;
    ImageView backButton;
    private JSONArray reviews = new JSONArray();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_schedule);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setStatusBarColor(Color.BLACK);
        }

        scheduleRecyclerView = (RecyclerView) findViewById(R.id.scheduleRecyclerView);
        backButton = (ImageView) findViewById(R.id.backButton);
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        getData(ViewSchedule.this);


    }

    public static void getData(final Context context) {
        ApiCall.getMethodHeaders(context, UrlHelper.GET_RECURRAL, new VolleyCallback() {
            @Override
            public void onSuccess(JSONObject response) {
                JSONArray jsonArray = response.optJSONArray("all_scheduled_bookings");

                LinearLayoutManager timeLayoutManager = new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false);

                scheduleRecyclerView.setLayoutManager(timeLayoutManager);
                RecurralAdapter reviewsAdapter = new RecurralAdapter(context, jsonArray);
                scheduleRecyclerView.setAdapter(reviewsAdapter);
            }
        });

    }


    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }
}
