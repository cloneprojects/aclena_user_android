package com.aclena.user.Volley;

import android.content.Context;
import android.content.Intent;

import com.android.volley.VolleyError;

import com.aclena.user.R;
import com.aclena.user.activities.AppSettings;
import com.aclena.user.activities.SignInActivity;
import com.aclena.user.application.Uberdoo;
import com.aclena.user.helpers.Utils;

/**
 * Created by user on 19-09-2017.
 */

public class VolleyErrorHandler {


    private static String TAG=VolleyErrorHandler.class.getSimpleName();

    public static void handle(String url, VolleyError volleyError)
    {
        Context context= Uberdoo.getContext();

        try {
            Utils.log(TAG, "handle:url "+volleyError.networkResponse.statusCode);

            switch (volleyError.networkResponse.statusCode) {
                case 500:
                    Utils.toast(context, context.getResources().getString(R.string.some_thing_went_wrong));
                    break;
                case 401:
                    moveToSignActivity(context);
                    Utils.toast(context,context.getResources().getString(R.string.please_login_again));
                    break;

            }
        }
        catch (Exception e)
        {
            Utils.toast(context, context.getResources().getString(R.string.some_thing_went_wrong));

            e.printStackTrace();
        }
    }

    private static void moveToSignActivity(Context context) {
        Intent intent=new Intent(context, SignInActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        AppSettings appSettings=new AppSettings(context);
        appSettings.setIsLogged("false");
        context.startActivity(intent);

    }


}
